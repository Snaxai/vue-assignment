# Vue Assignment Javascript Noroff
Second assignment for the javascript part of the java fullstack course at noroff in the experis academy program

The assignment is to create a trivia game where the user will login, select game options and get a score. This score will be saved in the user object. Every user and their highscore will also be saved in an API hosted on heroku.

## Component tree
A pdf of the component tree can be found in the component tree folder

## Hosted on Heroku
The application is hosted on heroku

`https://vue-assignment-dm-and-tde.herokuapp.com/`

## Author
Daniel Mossestad

Trym Dammann Ellingsen

