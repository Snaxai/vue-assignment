import { createStore } from "vuex"
import { apiFetchCategories, apiGenerateTrivia } from "./api/questions"
import { createNewUser, getUser, apiUpdateUserHighScore } from "./api/users"



const initialState = {
  user: JSON.parse(localStorage.getItem("trivia-user")) ?? null,
  questions: [],
  answers: [],
  categories: [],
  gameOptions: {
    categoryId: 10,
    numberOfQuestions: 1,
    difficulty: "",
  },
}

/**
 * The global state store of the project. Stores all global states in the application
 */
export default createStore({
  state: {
    user: JSON.parse(localStorage.getItem("trivia-user")) ?? null,
    questions: [],
    answers: [],
    categories: [],
    gameOptions: {
      categoryId: 0,
      numberOfQuestions: 0,
      difficulty: "easy",
    },
  },
  mutations: {
    setCategories: (state, categories) => {
      state.categories = categories
    },
    setQuestions: (state, questions) => {
      state.questions = questions
    },
    setAnswers: (state, answers) => {
      state.answers = answers
    },
    setUser: (state, user) => {
      state.user = user
    },
    setCategory: (state, categoryId) => {
      state.gameOptions.categoryId = categoryId
    },
    setNumberOfQuestions: (state, numberOfQuestions) => {
      state.gameOptions.numberOfQuestions = numberOfQuestions
    },
    setDifficulty: (state, difficulty) => {
      state.gameOptions.difficulty = difficulty
    },
  },
  actions: {
    async fetchAllCategories({ commit }) {
      const [error, categories] = await apiFetchCategories()

      if (error !== null) {
        return error
      }

      commit("setCategories", categories)
      return null
    },
    async createQuestions(
      { commit },
      { numOfQuestions, categoryId, difficulty }
    ) {
      const [error, questions] = await apiGenerateTrivia(
        numOfQuestions,
        categoryId,
        difficulty
      )

      if (error !== null) {
        return error
      }

      commit("setQuestions", questions)
      return null
    },
    loginWithUsername: async ({ commit }, { username }) => {
      const [error, user] = await getUser(username.value)

      if (error) {
        console.log("error in store", error)
        return error
      }

      commit("setUser", user[0])
      localStorage.setItem("trivia-user", JSON.stringify(user[0]))
    },
    createNewUser: async ({ commit }, { username }) => {
      const [existsError, data] = await getUser(username.value)

      if (username.value === "") {
        return "No username provided"
      }

      if (data.length != 0) {
        return `Cannot register user: ${username.value} already registered`
      }

      const [error, user] = await createNewUser(username.value)

      if (error) {
        console.log("error in store", error)
        return error
      }

      commit("setUser", user)
      localStorage.setItem("trivia-user", JSON.stringify(user))
    },
    updateHighScore: async ({ commit }, { userId, score }) => {
      console.log("ID: " + userId + " score: " + score)
      const [error, updatedUser] = await apiUpdateUserHighScore(userId, score)
      console.log(updatedUser)
      localStorage.setItem("trivia-user", JSON.stringify(updatedUser))
      if (!error) {
        commit("setUser", updatedUser)
      }
    },
    logout: ({ commit }) => {
      localStorage.clear()
      commit("setUser", initialState.user)
      commit("setQuestions", initialState.questions)
      commit("setAnswers", initialState.answers)
    },
  },
})
