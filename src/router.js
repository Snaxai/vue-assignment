import { createRouter, createWebHistory } from "vue-router";
import store from "./store";
import Start from "./views/Start.vue";
import Questions from "./views/Questions.vue";
import Results from "./views/Results.vue";

/**
 * Authentication guard. Will redirect user to start if not logged in
 * @param {*} _to 
 * @param {*} _from 
 * @param {*} next Where the user is redirected to
 */
const authGuard = (_to, _from, next) => {
  if (!store.state.user) {
    next("/");
  } else {
    next();
  }
};

/* const loginGuard = (_to, _from, next) => {
  if (store.state.user) next("/questions/0");
  else next();
}; */

// The different routes in the Application
const routes = [
  {
    path: "/",
    component: Start,
  },
  {
    path: "/results",
    component: Results,
    beforeEnter: authGuard,
  },
  {
    path: "/questions/:questionId",
    component: Questions,
    beforeEnter: authGuard,
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
