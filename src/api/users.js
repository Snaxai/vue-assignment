import { BASE_API_URL } from "."
import { API_KEY } from "."

/**
 * Gets the user info based on a specific username
 * @param {*} username The username of the user
 * @returns Error or a user object
 */
export async function getUser(username) {
  try {
    const response = await fetch(`${BASE_API_URL}/trivia?username=${username}`)
    if (!response.ok) {
      throw new Error("no response")
    }
    const data = await response.json()

    if (data <= 0) throw new Error(`No user with username: ${username}`)

    return [null, data]
  } catch (error) {
    return [error.message, []]
  }
}

/**
 * Creates a new user
 * @param {*} username The username of a user
 * @returns Error or the new user created
 */
export async function createNewUser(username) {
  try {
    const response = await fetch(`${BASE_API_URL}/trivia`, {
      method: "POST",
      headers: {
        "X-API-Key": API_KEY,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        highScore: 0,
      }),
    })
    if (!response.ok) {
      throw new Error("no response")
    }

    const data = await response.json()

    return [null, data]
  } catch (error) {
    console.log("Error occured. caught in trycatch in users.js")
    return [error.message, []]
  }
}

/**
 * Updates the highscore of a user
 * @param {*} userId The users id
 * @param {*} score The new highscore
 * @returns Error or the user object that was updated
 */
export async function apiUpdateUserHighScore(userId, score) {
  try {
    const response = await fetch(`${BASE_API_URL}/trivia/${userId}`, {
      method: "PATCH",
      headers: {
        "X-API-KEY": API_KEY,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        highScore: score,
      }),
    })

    console.log("completed update")

    if (!response.ok) {
      throw new Error("Something went wrong updating user score")
    }

    const updatedUser = await response.json()

    console.log(updatedUser)

    return [null, updatedUser]
  } catch (error) {
    return [error.message, null]
  }
}
