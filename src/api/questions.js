import { TRIVIA_BASE_URL } from "."

/**
 * Fetches the possible categories for generating trivia from the trivia api
 * @returns error or the possible trivia categories
 */
export async function apiFetchCategories() {
    try {
        const response = await fetch(`${TRIVIA_BASE_URL}_category.php`)

        if (!response.ok) {
            throw new Error("Something went wrong while fetching categories")
        }

        const { trivia_categories } = await response.json()

        return [null, trivia_categories]
    } catch (error) {
        console.log(error.message)
        return [error.message, null]
    }
}

/**
 * Generates the questions for the quiz based on the parameters.
 * @param {*} numOfQuestions the number of questions to generate. Anything higher than 50 is equivalent to 50
 * @param {*} categoryId the id of the category to generate questions for. Can be between 9 and 31 (at time of writing)
 * @param {*} difficulty the difficulty of the questions. Easy, medium or hard.
 * @returns an error message or the generated questions.
 */
export async function apiGenerateTrivia(
    numOfQuestions,
    categoryId,
    difficulty
) {
    try {
        // Gets $numOfQuestions questions of $categoryId category and $difficulty difficulty
        // Will get both boolean and multiple choice questions
        const response = await fetch(
            TRIVIA_BASE_URL +
                `.php?amount=${numOfQuestions}&category=${categoryId}&difficulty=${difficulty}`
        )

        // If we don't know exactly what the error is, use this
        const defaultError = "Something went wrong generating the quiz"

        if (!response.ok) {
            throw new Error(defaultError)
        }

        const { response_code, results } = await response.json()

        console.log(response_code)

        // Defined by the trivia API
        if (response_code == 1) {
            throw new Error(
                "Not enough questions available in category." +
                    " Try lowering the number of questions or choose a different category or difficulty"
            )
        } else if (response_code) {
            throw new Error(defaultError)
        }

        return [null, results]
    } catch (error) {
        return [error.message, null]
    }
}
